﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEvo2 : MonoBehaviour
{
    public bool pressed = false;
    public bool seguir = true;
    //public GameObject 

    public GameObject Moneda;

    // Validar que la posición del objecto se capture 
    bool valPos = false;
    // Start is called before the first frame update

    //Variables de reposicionamiento
    public bool RePosition = false;
    public bool DentroLimite = false;
    public GameObject targ;

    public float VarChangeAnimation = 0f;

    void Start()
    {
        StartCoroutine(ChangeAnimation());
        StartCoroutine(generadorOro());
        targ = GameObject.Find("Centro");
    }

    // Update is called once per frame
    void Update()
    {
        if (pressed == true)
        {

            this.GetComponent<SpriteRenderer>().sortingOrder = 3;

            //Set the position to the mouse position
            this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y,
                0.0f);
        }

        //Gets the world position of the mouse on the screen        
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Checks whether the mouse is over the sprite
        bool overSprite = this.GetComponent<SpriteRenderer>().bounds.Contains(mousePosition);

        //If it's over the sprite
        if (overSprite)
        {
            //If we've pressed down on the mouse (or touched on the iphone)
        }

        if (Input.GetMouseButtonUp(0) && seguir == true)
        {
            pressed = false;
            this.GetComponent<SpriteRenderer>().sortingOrder = 3;
            valPos = false;
        }

        if (Input.GetMouseButtonUp(0) && DentroLimite == true)
        {
            RePosition = true;
        }

        if (RePosition == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targ.transform.position, .40f);
        }

        if (transform.position == targ.transform.position)
        {
            DentroLimite = false;
            RePosition = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Limite")
        {
            DentroLimite = true;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && seguir == true)
        {
            pressed = true;

            if (valPos == false)
            {
                //positionObj = this.gameObject.transform;
                //Debug.Log(positionObj.position);
                valPos = true;
            }
        }
    }


    IEnumerator generadorOro()
    {

        yield return new WaitForSeconds(2.5f);
        Moneda.GetComponent<Animator>().SetBool("active", false);
        yield return new WaitForSeconds(2.5f);
        GameObject.Find("ControladorEventos").GetComponent<EventController>().Oro += 15;
        Moneda.GetComponent<Animator>().SetBool("active", true);
        Debug.Log(GameObject.Find("ControladorEventos").GetComponent<EventController>().Oro);

        StartCoroutine(generadorOro());
    }

    IEnumerator ChangeAnimation()
    {
        VarChangeAnimation = Random.Range(0f, 5f);
        yield return new WaitForSeconds(VarChangeAnimation);
        GetComponent<Animator>().SetBool("ChangeIdle", true);
        yield return new WaitForSeconds(0.3f);
        GetComponent<Animator>().SetBool("ChangeIdle", false);
        StartCoroutine(ChangeAnimation());
    }

}
